:warning: this role is deprecated and you are encouraged to use (parameter_store_to_config)[https://gitlab.com/myelefant1/ansible-roles3/parameter_store_to_config]

# Parameter store to ini

This role:
  * installs awscli and jq
  * installs service parameter_store_to_ini to generate a file ini

## Role parameters

| name                    | value       | default_value   | description                                 |
| ------------------------|-------------|-----------------|---------------------------------------------|
| parameter_store_to_ini_service_state           | string      | started         | State of the systemd service after the run               |
| parameter_store_to_ini_aws_region              | string      | false           | AWS region where ParameterStore is configured            |
| parameter_store_to_ini_parameter_path          | string      | false           | Path for settings in ParameterStore                      |
| parameter_store_to_ini_parameter_common_path   | string      | false           | Path for common settings in ParameterStore (optional)    |
| parameter_store_to_ini_parameter_store_output  | string      | false           | Location of generated ini file                           |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/parameter-store-to-ini.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: parameter-store-to-ini
      parameter_store_to_ini_aws_region: us-east-1
      parameter_store_to_ini_parameter_path: /production
      parameter_store_to_ini_parameter_store_output: /tmp/settings.ini
```

